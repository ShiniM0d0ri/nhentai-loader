import os
from flask import Flask, render_template, request,send_from_directory
import fetch

app = Flask(__name__)


@app.route('/')
def index():
   return render_template('index.html')

#favicon route
@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/result',methods = ['POST', 'GET'])
def result():
   imglist=[]
   if request.method == 'POST':
      magicno = request.form['magicno']
      imglist=fetch.main(magicno)
      return render_template("result.html", magicno=magicno, imglist=imglist)


if __name__ == '__main__':
   app.run(debug = True)